const logger = require('../logs/logger');

const handlesException = (error, origin) => {
    logger.error('Uncaught exception, shutting down...', {
        error: error,
        origin: origin,
        status: error.status,
        messages: error.messages,
        name: error.name,
        code: error.code,
        stack: error.stack
    });

    process.exitCode = 1;
};

module.exports = handlesException;
