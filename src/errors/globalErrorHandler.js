const AppError = require('./appError');
const logger = require('../logs/logger');

const handlesDuplicateFieldsDB = (error) => {
    const messages = error.meta.target.map((field) => {
        return `Duplicate field : ${field}. Please use another value !`;
    });

    return new AppError(messages, 400);
};

const handlesRecordNotFoundDB = () => {
    const message = ['Record not found ! Please, use another id.'];

    return new AppError(message, 400);
};

const handlesJWTError = () => {
    const message = ['Invalid token. Please log in again !'];

    return new AppError(message, 401);
};

const handlesJWTExpiredError = () => {
    const message = ['Your token has expired! Please log in again.'];

    return new AppError(message, 401);
};

const sendErrorDev = (err, req, res) => {
    res.status(err.statusCode).json({
        status: err.status,
        error: err,
        messages: err.messages,
        stack: err.stack
    });
};

const sendErrorProd = (err, req, res) => {
    if (err.isOperational) {
        return res.status(err.statusCode).json({
            status: err.status,
            messages: err.messages
        });
    }

    logger.error('Not operational', {
        request_id: req.id,
        status: err.status,
        error: err,
        messages: err.messages,
        name: err.name,
        code: err.code,
        stack: err.stack
    });

    res.status(500).json({
        status: 'error',
        messages: [
            { text: 'Something went very wrong !' }
        ]
    });
};

module.exports = (err, req, res, next) => {
    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error';

    if (res.headersSent) return next(err);

    if (process.env.NODE_ENV === 'development') {
        return sendErrorDev(err, req, res);
    }

    let error = err;

    if (error.code === 'P2002') error = handlesDuplicateFieldsDB(error);

    if (error.code === 'P2025') error = handlesRecordNotFoundDB();

    if (error.name === 'JsonWebTokenError') error = handlesJWTError();

    if (error.name === 'TokenExpiredError') error = handlesJWTExpiredError();

    sendErrorProd(error, req, res);
};
