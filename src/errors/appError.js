class AppError extends Error {
    constructor(messages, statusCode) {
        super();
        
        this.messages = messages.map((message) => ({ text: message }));

        this.statusCode = statusCode;

        this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';

        this.isOperational = true;

        Error.captureStackTrace(this, this.constructor);
    }
}

module.exports = AppError;