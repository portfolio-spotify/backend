const logger = require('../logs/logger');

module.exports = (err, req, res, next) => {
    logger.error('Application', {
        request_id: req.id,
        status: err.status,
        messages: err.messages,
        name: err.name,
        code: err.code,
        error: err,
        stack: err.stack,
    });

    next(err);
};
