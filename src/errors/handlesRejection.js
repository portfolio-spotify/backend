const logger = require('../logs/logger');

const handlesRejection = (reason, promise) => {
    logger.error('Unhandled rejection, shutting down...', {
        error: reason,
        promise: promise,
        status: reason.status,
        messages: reason.messages,
        name: reason.name,
        code: reason.code,
        stack: reason.stack
    });

    process.exitCode = 1;
};

module.exports = handlesRejection;