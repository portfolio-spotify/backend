const { promisify } = require('node:util');
const jwt = require('jsonwebtoken');

const User = require('../models/userModel');

const AppError = require('../errors/appError');
const catchAsync = require('../utils/catchAsync');

exports.protect = catchAsync(async (req, res, next) => {
    const token = req.cookies.jwt;

    if (!token) {
        return next(new AppError([`You're not allowed to access this route, please authenticate to get access !`], 401));
    }

    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

    const currentUser = await User.findOne(decoded.url);

    if (!currentUser) {
        return next(new AppError(['The user belonging to this token does no longer exist.'], 401));
    }

    req.user = currentUser;

    return next();
});
