const Track = require('../models/trackModel');

const catchAsync = require('../utils/catchAsync');

exports.getPopularTracksFromUser = catchAsync(async (req, res) => {
    const limit = req.query.limit;
    const { record } = req;

    const popular = await Track.findMostListenedByUser(record.url, limit);

    return res.status(200).json({
        status: 'success',
        data: popular,
    });
});
