const multer = require('multer');
const multerS3 = require('multer-s3');

const { S3Client } = require('../configs/aws');

const Project = require('../models/projectModel');
const AWSS3 = require('../models/awsS3Model');
const ProjectTechnologies = require('../models/projectTechnologyModel');

const catchAsync = require('../utils/catchAsync');
const logger = require('../logs/logger');
const validation = require('../utils/validation');
const AppError = require('../errors/appError');
const factory = require('../controllers/factory');
const { generateId } = require('../utils/id');

const getBucketAndKey = (image) => {
    const bucket = process.env.AWS_S3_BUCKET_NAME;
    const key = `${process.env.AWS_S3_PROJECTS_IMAGES_PATH}${image}`;

    return { bucket, key };
};

const awsStorage = multerS3({
    s3: S3Client,
    bucket: `${process.env.AWS_S3_BUCKET_NAME}`,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: (req, file, cb) => {
        cb(null, {
            fieldName: file.fieldname, 
        });
    },
    key: (req, file, cb) => {
        const extension = file.mimetype.split('/')[1];

        const id = generateId(32);
        
        const newFileName = `${id}.${extension}`;
        
        const path = `${process.env.AWS_S3_PROJECTS_IMAGES_PATH}${newFileName}`;
        
        file.newFileName = newFileName;

        cb(null, path);
    }
});

const multerLimits = {
    fields: 5,
    files: 1,
    fileSize: process.env.SIZE_LIMIT_FILE_UPLOAD,
};

const multerFileFilter = (req, file, cb) => {
    const allowedExtension = ['image/jpeg', 'image/png'];

    const { mimetype } = file;

    if (allowedExtension.includes(mimetype)) return cb(null, true);

    cb(new multer.MulterError('CUSTOM_UNEXPECTED_MIMETYPE'));
};

const upload = multer({ 
    storage: awsStorage,
    limits: multerLimits,
    fileFilter: multerFileFilter
}).single('image');

exports.getAllProjects = factory.getAll(Project);

exports.verifyProject = factory.verifyOne(Project);

exports.getProject = factory.getOne;

exports.uploadProjectImage = (req, res, next) => {
    req.fileUploadSizeLimit = process.env.SIZE_LIMIT_FILE_UPLOAD || 30_000_000;

    upload(req, res, (err) => {
        req.errorFiles = [];

        if (!err) return next();

        logger.error(`Upload with Multer`, {
            request_id: req.id,
            status: err.status,
            messages: err.messages,
            name: err.name,
            code: err.code,
            error: err,
            stack: err.stack
        });

        if (!(err instanceof multer.MulterError)) {
            return next(new AppError([`We weren't able to upload your file !`], 500));
        }

        if (err.code === 'CUSTOM_UNEXPECTED_MIMETYPE') {
            req.errorFiles.push('Only the following formats are allowed : .jpeg, .jpg, .png');

            return next();
        }

        if (err.code === 'LIMIT_PART_COUNT') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FILE_SIZE') {
            const kiloBytesLimit = req.fileUploadSizeLimit / 1_000;

            req.errorFiles.push(`File size too large, the limit is : ${kiloBytesLimit}kb`);

            return next();
        }

        if (err.code === 'LIMIT_FILE_COUNT') {
            req.errorFiles.push(`You're sending too many files !`);

            return next();
        }

        if (err.code === 'LIMIT_FIELD_KEY') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FIELD_VALUE') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FIELD_COUNT') {
            req.errorFiles.push(`You're sending too many fields !`);

            return next();
        }
    
        if (err.code === 'LIMIT_UNEXPECTED_FILE') {
            req.errorFiles.push(`You're sending an unexpected file !`);

            return next();
        }
    
        if (err.code === 'MISSING_FIELD_NAME') return next();

        return next(new AppError(['Something went wrong during the upload !'], 500));
    });
};

exports.createProject = catchAsync(async (req, res, next) => {
    const errorMessages = validation.results(req);

    const { file, errorFiles } = req;

    if (!errorFiles.length && !file) {
        errorMessages.push('An image is required with your project !');
    }

    if (errorFiles.length) errorMessages.push(...errorFiles);

    if (errorMessages.length && file) {
        const deleteResponse = await AWSS3.deleteObject(file.bucket, file.key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorMessages.length) return next(new AppError(errorMessages, 400));

    req.body.image = file.newFileName;
    req.body.author_id = req.user.id;

    const project = await Project.createOne(req.body);
    await ProjectTechnologies.createMany(project.id, req.body.technologiesIds);

    logger.info(`Project created with id : ${project.id}`, {
        request_id: req.id
    });

    return res.status(201).json({
        status: 'success',
        data: project
    });
});

exports.deleteProject = catchAsync(async (req, res) => {
    const project = await Project.deleteOne(req.params.id);

    const { bucket, key } = getBucketAndKey(project.image);

    const deleteResponse = await AWSS3.deleteObject(bucket, key);

    AWSS3.logResponse(deleteResponse, req.id);

    return res.status(204).json({
        status: 'success',
        data: null
    });
});

exports.updateProject = catchAsync(async (req, res, next) => {
    const { file, errorFiles, record: oldProject } = req;

    if (errorFiles.length && file) {
        const deleteResponse = await AWSS3.deleteObject(file.bucket, file.key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorFiles.length) return next(new AppError(errorFiles, 400));

    const { title, description, color, url } = req.body;
    const data = {};

    if (title) data.title = title;
    if (description) data.description = description;
    if (color) data.color = color;
    if (url) data.url = url;
    if (file) data.image = file.newFileName;

    if (file) {
        const { bucket, key } = getBucketAndKey(oldProject.image);

        const deleteResponse = await AWSS3.deleteObject(bucket, key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    const project = await Project.updateOne(req.params.id, data);

    return res.status(200).json({
        status: 'success',
        data: project
    });
});
