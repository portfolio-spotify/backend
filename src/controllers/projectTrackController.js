const Track = require('../models/trackModel');

const catchAsync = require('../utils/catchAsync');

exports.getAllTracksFromProject = catchAsync(async (req, res) => {
    const projectsTracks = await Track.findManyFromProject(req.record.id);
    
    return res.status(200).json({
        status: 'success',
        data: projectsTracks
    });
});