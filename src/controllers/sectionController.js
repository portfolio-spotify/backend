const Project = require('../models/projectModel');

const catchAsync = require('../utils/catchAsync');

exports.getAllSections = catchAsync(async (req, res) => {
    const limit = req.query.limit;

    const trendingPromise = Project.findRandom(limit);
    const latestPromise = Project.findLatest(limit);
    const discoverPromise = Project.findDiscover(limit);
    const selectedPromise = Project.findRandom(limit);

    const promises = [trendingPromise, latestPromise, selectedPromise, discoverPromise];

    const [trending, latest, selected, discover] = await Promise.all(promises);

    res.status(200).json({
        status: 'success',
        data: {
            trending,
            latest,
            selected,
            discover
        }
    });
});

exports.getTrendingSection = catchAsync(async (req, res) => {
    const trending = await Project.findRandom();

    res.status(200).json({
        status: 'success',
        data: trending
    });
});

exports.getLatestSection = catchAsync(async (req, res) => {
    const latest = await Project.findLatest();

    res.status(200).json({
        status: 'success',
        data: latest
    });
});

exports.getDiscoverSection = catchAsync(async (req, res) => {
    const discover = await Project.findDiscover();

    res.status(200).json({
        status: 'success',
        data: discover
    });
});

exports.getSelectedSection = catchAsync(async (req, res) => {
    const selected = await Project.findRandom();

    res.status(200).json({
        status: 'success',
        data: selected
    });
});
