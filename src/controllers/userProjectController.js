const Project = require('../models/projectModel');

const catchAsync = require('../utils/catchAsync');

exports.getPopularProjectsFromUser = catchAsync(async (req, res) => {
    const limit = req.query.limit;
    const { record } = req;

    const popular = await Project.findMostListenedByUser(record.url, limit);

    return res.status(200).json({
        status: 'success',
        data: popular,
    });
});

exports.getNewProjectsFromUser = catchAsync(async (req, res) => {
    const limit = req.query.limit;
    const { record } = req;

    const latest = await Project.findLatestByUser(record.url, limit);

    return res.status(200).json({
        status: 'success',
        data: latest
    });
});

exports.getTotalProjects = catchAsync(async (req, res) => {
    const { record } = req;

    const total = await Project.findTotalByUser(record.url);

    return res.status(200).json({
        status: 'success',
        data: total
    });
});
