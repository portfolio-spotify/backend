const multer = require('multer');
const multerS3 = require('multer-s3');

const { S3Client } = require('../configs/aws');

const User = require('../models/userModel');
const AWSS3 = require('../models/awsS3Model');

const AppError = require('../errors/appError');
const catchAsync = require('../utils/catchAsync');
const logger = require('../logs/logger');
const validation = require('../utils/validation');
const factory = require('../controllers/factory');
const { generateId } = require('../utils/id');

const getBucketAndKey = (image) => {
    const bucket = process.env.AWS_S3_BUCKET_NAME;
    const key = `${process.env.AWS_S3_USERS_IMAGES_PATH}${image}`;

    return { bucket, key };
};

const awsStorage = multerS3({
    s3: S3Client,
    bucket: `${process.env.AWS_S3_BUCKET_NAME}`,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: (req, file, cb) => {
        cb(null, {
            fieldName: file.fieldname, 
        });
    },
    key: (req, file, cb) => {
        const extension = file.mimetype.split('/')[1];

        const id = generateId(32);
        
        const newFileName = `${id}.${extension}`;
        
        const path = `${process.env.AWS_S3_USERS_IMAGES_PATH}${newFileName}`;
        
        file.newFileName = newFileName;

        cb(null, path);
    }
});

const multerLimits = {
    fields: 5,
    files: 3,
    fileSize: process.env.SIZE_LIMIT_FILE_UPLOAD,
};

const multerFileFilter = (req, file, cb) => {
    const allowedExtension = ['image/jpeg', 'image/png'];

    const { mimetype } = file;

    if (allowedExtension.includes(mimetype)) return cb(null, true);

    cb(new multer.MulterError('CUSTOM_UNEXPECTED_MIMETYPE'));
};

const imagesList = [
    {
        name: 'imageProfile',
        maxCount: 1
    },
    {
        name: 'imageCover',
        maxCount: 1
    },
    {
        name: 'imageDescription',
        maxCount: 1
    }
];

const upload = multer({ 
    storage: awsStorage,
    limits: multerLimits,
    fileFilter: multerFileFilter
}).fields(imagesList);

exports.getAllUsers = factory.getAll(User);

exports.createUser = catchAsync(async (req, res, next) => {
    const errorMessages = validation.results(req);

    const { errorFiles } = req;
    const { imageProfile, imageCover, imageDescription } = req.files;

    if (!errorFiles.length && !imageProfile) {
        errorMessages.push('A profile image is required !');
    }

    if (!errorFiles.length && !imageCover) {
        errorMessages.push('A cover image is required !');
    }

    if (!errorFiles.length && !imageDescription) {
        errorMessages.push('A description image is required !');
    }

    if (errorFiles.length) errorMessages.push(...errorFiles);

    if (errorMessages.length && imageProfile) {
        const deleteResponse = await AWSS3.deleteObject(imageProfile[0].bucket, imageProfile[0].key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorMessages.length && imageCover) {
        const deleteResponse = await AWSS3.deleteObject(imageCover[0].bucket, imageCover[0].key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorMessages.length && imageDescription) {
        const deleteResponse = await AWSS3.deleteObject(imageDescription[0].bucket, imageDescription[0].key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorMessages.length) return next(new AppError(errorMessages, 400));

    req.body.imageProfile = imageProfile[0].newFileName;
    req.body.imageCover = imageCover[0].newFileName;
    req.body.imageDescription = imageDescription[0].newFileName;

    const user = await User.createOne(req.body);

    logger.info(`User created with id : ${user.id}`, {
        request_id: req.id
    });

    return res.status(201).json({
        status: 'success',
        data: user
    });
});

exports.getUser = factory.getOne;

exports.updateUser = catchAsync(async (req, res, next) => {
    const { errorFiles, record: oldUser } = req;

    const { imageProfile, imageCover, imageDescription } = req.files;

    if (errorFiles.length && imageProfile) {
        const deleteResponse = await AWSS3.deleteObject(imageProfile[0].bucket, imageProfile[0].key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorFiles.length && imageCover) {
        const deleteResponse = await AWSS3.deleteObject(imageCover[0].bucket, imageCover[0].key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorFiles.length && imageDescription) {
        const deleteResponse = await AWSS3.deleteObject(imageDescription[0].bucket, imageDescription[0].key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorFiles.length) return next(new AppError(errorFiles, 400));

    const { firstName, lastName, description, url, color } = req.body;
    const data = {};

    if (firstName) data.firstName = firstName;
    if (lastName) data.lastName = lastName;
    if (description) data.description = description;
    if (url) data.url = url;
    if (color) data.color = color;
    if (imageProfile) data.imageProfile = imageProfile[0].newFileName;
    if (imageCover) data.imageCover = imageCover[0].newFileName;
    if (imageDescription) data.imageDescription = imageDescription[0].newFileName;

    if (imageProfile) {
        const { bucket, key } = getBucketAndKey(oldUser.imageProfile);

        const deleteResponse = await AWSS3.deleteObject(bucket, key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (imageCover) {
        const { bucket, key } = getBucketAndKey(oldUser.imageCover);

        const deleteResponse = await AWSS3.deleteObject(bucket, key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (imageDescription) {
        const { bucket, key } = getBucketAndKey(oldUser.imageDescription);

        const deleteResponse = await AWSS3.deleteObject(bucket, key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    const user = await User.updateOne(req.params.id, data);

    return res.status(200).json({
        status: 'success',
        data: user
    });
});

exports.verifyUser = factory.verifyOne(User);

exports.uploadUserImages = (req, res, next) => {
    req.fileUploadSizeLimit = process.env.SIZE_LIMIT_FILE_UPLOAD || 30_000_000;

    upload(req, res, (err) => {
        req.errorFiles = [];

        if (!err) return next();

        logger.error(`Upload with Multer`, {
            request_id: req.id,
            status: err.status,
            messages: err.messages,
            name: err.name,
            code: err.code,
            error: err,
            stack: err.stack
        });

        if (!(err instanceof multer.MulterError)) {
            return next(new AppError([`We weren't able to upload your files !`], 500));
        }

        if (err.code === 'CUSTOM_UNEXPECTED_MIMETYPE') {
            req.errorFiles.push('Only the following formats are allowed : .jpeg, .jpg, .png');

            return next();
        }

        if (err.code === 'LIMIT_PART_COUNT') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FILE_SIZE') {
            const kiloBytesLimit = req.fileUploadSizeLimit / 1_000;

            req.errorFiles.push(`Files size too large, the limit is : ${kiloBytesLimit}kb`);

            return next();
        }

        if (err.code === 'LIMIT_FILE_COUNT') {
            req.errorFiles.push(`You're sending too many files !`);

            return next();
        }

        if (err.code === 'LIMIT_FIELD_KEY') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FIELD_VALUE') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FIELD_COUNT') {
            req.errorFiles.push(`You're sending too many fields !`);

            return next();
        }
    
        if (err.code === 'LIMIT_UNEXPECTED_FILE') {
            req.errorFiles.push(`You're sending an unexpected file !`);

            return next();
        }
    
        if (err.code === 'MISSING_FIELD_NAME') return next();

        return next(new AppError(['Something went wrong during the upload !'], 500));
    });
};
