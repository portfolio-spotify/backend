const Project = require('../models/projectModel');

const catchAsync = require('../utils/catchAsync');

exports.getPopularProjectsFromTechnology = catchAsync(async (req, res) => {
    const limit = req.query.limit;
    const { record } = req;

    const popular = await Project.findMostListenedByTechnology(record.url, limit);

    return res.status(200).json({
        status: 'success',
        data: popular,
    });
});

exports.getNewProjectsFromTechnology = catchAsync(async (req, res) => {
    const limit = req.query.limit;
    const { record } = req;

    const latest = await Project.findLatestByTechnology(record.url, limit);

    return res.status(200).json({
        status: 'success',
        data: latest
    });
});
