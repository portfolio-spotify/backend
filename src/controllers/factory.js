const AppError = require('../errors/appError');
const catchAsync = require('../utils/catchAsync');

exports.verifyOne = (Model) => catchAsync(async (req, res, next) => {
    const record = await Model.findOne(req.params.id);

    if (!record) return next(new AppError(['No record found with that id!'], 404));

    req.record = record;

    return next();
});

exports.getOne = (req, res) => {
    return res.status(200).json({
        status: 'success',
        data: req.record
    });
};

exports.getAll = (Model) => catchAsync(async (req, res) => {
    const records = await Model.findMany();

    return res.status(200).json({
        status: 'success',
        data: records
    });
});
