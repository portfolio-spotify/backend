const multer = require('multer');
const multerS3 = require('multer-s3');
const { parseBuffer } = require('music-metadata');

const { S3Client } = require('../configs/aws');

const Track = require('../models/trackModel');
const AWSS3 = require('../models/awsS3Model');

const factory = require('../controllers/factory');
const validation = require('../utils/validation');
const AppError = require('../errors/appError');
const logger = require('../logs/logger');
const { generateId } = require('../utils/id');
const catchAsync = require('../utils/catchAsync');

const getBucketAndKey = (file) => {
    const bucket = process.env.AWS_S3_BUCKET_NAME;
    const key = `${process.env.AWS_S3_TRACKS_PATH}${file}`;

    return { bucket, key };
};

const awsStorage = multerS3({
    s3: S3Client,
    bucket: `${process.env.AWS_S3_BUCKET_NAME}`,
    contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: (req, file, cb) => {
        cb(null, {
            fieldName: file.fieldname, 
        });
    },
    key: (req, file, cb) => {
        const extension = file.mimetype.split('/')[1];

        const id = generateId(32);
        
        const newFileName = `${id}.${extension}`;
        
        const path = `${process.env.AWS_S3_TRACKS_PATH}${newFileName}`;
        
        file.newFileName = newFileName;

        cb(null, path);
    }
});

const multerLimits = {
    fields: 3,
    files: 1,
    fileSize: process.env.SIZE_LIMIT_AUDIO_UPLOAD,
};

const multerFileFilter = (req, file, cb) => {
    const allowedExtension = ['audio/mpeg'];

    const { mimetype } = file;

    if (allowedExtension.includes(mimetype)) return cb(null, true);

    cb(new multer.MulterError('CUSTOM_UNEXPECTED_MIMETYPE'));
};

const upload = multer({ 
    storage: awsStorage,
    limits: multerLimits,
    fileFilter: multerFileFilter
}).single('audio');

exports.getAllTracks = factory.getAll(Track);

exports.getTrack = factory.getOne;

exports.verifyTrack = factory.verifyOne(Track);

exports.uploadTrackAudio = (req, res, next) => {
    req.fileUploadSizeLimit = process.env.SIZE_LIMIT_AUDIO_UPLOAD || 60_000_000;

    upload(req, res, (err) => {
        req.errorFiles = [];

        if (!err) return next();

        logger.error(`Upload with Multer`, {
            request_id: req.id,
            status: err.status,
            messages: err.messages,
            name: err.name,
            code: err.code,
            error: err,
            stack: err.stack
        });

        if (!(err instanceof multer.MulterError)) {
            return next(new AppError([`We weren't able to upload your file !`], 500));
        }

        if (err.code === 'CUSTOM_UNEXPECTED_MIMETYPE') {
            req.errorFiles.push('Only the following formats are allowed : .mp3');

            return next();
        }

        if (err.code === 'LIMIT_PART_COUNT') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FILE_SIZE') {
            const kiloBytesLimit = req.fileUploadSizeLimit / 1_000;

            req.errorFiles.push(`File size too large, the limit is : ${kiloBytesLimit}kb`);

            return next();
        }

        if (err.code === 'LIMIT_FILE_COUNT') {
            req.errorFiles.push(`You're sending too many files !`);

            return next();
        }

        if (err.code === 'LIMIT_FIELD_KEY') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FIELD_VALUE') {
            return next(new AppError(['An error occured while processing your data !'], 500));
        }

        if (err.code === 'LIMIT_FIELD_COUNT') {
            req.errorFiles.push(`You're sending too many fields !`);

            return next();
        }
    
        if (err.code === 'LIMIT_UNEXPECTED_FILE') {
            req.errorFiles.push(`You're sending an unexpected file !`);

            return next();
        }
    
        if (err.code === 'MISSING_FIELD_NAME') return next();

        return next(new AppError(['Something went wrong during the upload !'], 500));
    });
};

exports.createTrack = catchAsync(async (req, res, next) => {
    const errorMessages = validation.results(req);

    const { file, errorFiles } = req;

    if (!errorFiles.length && !file) {
        errorMessages.push('An audio file is required with each track !');
    }

    if (errorFiles.length) errorMessages.push(...errorFiles);

    if (errorMessages.length && file) {
        const deleteResponse = await AWSS3.deleteObject(file.bucket, file.key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorMessages.length) return next(new AppError(errorMessages, 400));

    req.body.audio_file = file.newFileName;

    const { bucket, key } = getBucketAndKey(file.newFileName);

    const s3Object = await AWSS3.getObject(bucket, key);

    const stream = s3Object.Body;

    const dataBuffer = await new Promise((resolve, reject) => {
        const chunks = []
        stream.on('data', (chunk) => chunks.push(chunk))
        stream.once('end', () => resolve(Buffer.concat(chunks)))
        stream.once('error', reject)
    });

    const metadata = await parseBuffer(dataBuffer, { mimeType: file.mimetype });

    req.body.duration = metadata.format.duration;

    const track = await Track.createOne(req.body);

    logger.info(`Track created with id : ${track.id}`, {
        request_id: req.id
    });

    return res.status(201).json({
        status: 'success',
        data: track
    });
});

exports.deleteTrack = catchAsync(async (req, res) => {
    const track = await Track.deleteOne(req.params.id);

    const { bucket, key } = getBucketAndKey(track.audio_file);

    const deleteResponse = await AWSS3.deleteObject(bucket, key);

    AWSS3.logResponse(deleteResponse, req.id);

    return res.status(204).json({
        status: 'success',
        data: null
    });
});

exports.updateTrack = catchAsync(async (req, res, next) => {
    const { file, errorFiles, record: oldTrack } = req;

    if (errorFiles.length && file) {
        const deleteResponse = await AWSS3.deleteObject(file.bucket, file.key);

        AWSS3.logResponse(deleteResponse, req.id);
    }

    if (errorFiles.length) return next(new AppError(errorFiles, 400));

    const { title, projectId, orderNumber } = req.body;
    const data = {};

    if (title) data.title = title;
    if (projectId) data.project_id = projectId;
    if (orderNumber) data.order_number = orderNumber;

    if (file) {
        const { bucket, key } = getBucketAndKey(oldTrack.audio_file);

        const deleteResponse = await AWSS3.deleteObject(bucket, key);

        AWSS3.logResponse(deleteResponse, req.id);

        data.audio_file = file.newFileName;

        const { key: newFileKey } = getBucketAndKey(file.newFileName);

        const s3Object = await AWSS3.getObject(bucket, newFileKey);

        const stream = s3Object.Body;

        const dataBuffer = await new Promise((resolve, reject) => {
            const chunks = []
            stream.on('data', (chunk) => chunks.push(chunk))
            stream.once('end', () => resolve(Buffer.concat(chunks)))
            stream.once('error', reject)
        });

        const metadata = await parseBuffer(dataBuffer, { mimeType: file.mimetype });

        req.body.duration = metadata.format.duration;
    }

    const track = await Track.updateOne(req.params.id, data);

    return res.status(200).json({
        status: 'success',
        data: track
    });
});
