const ProjectTechnologies = require('../models/projectTechnologyModel');

const AppError = require('../errors/appError');
const validation = require('../utils/validation');
const catchAsync = require('../utils/catchAsync');

exports.addTechnologiesToProject = catchAsync(async (req, res, next) => {
    const errorMessages = validation.results(req);

    if (errorMessages.length) return next(new AppError(errorMessages, 400));

    const { technologiesIds } = req.body;

    const projectsTechnologies = await ProjectTechnologies.createMany(req.record.id, technologiesIds);

    return res.status(201).json({
        status: 'success',
        data: projectsTechnologies
    });
});

exports.deleteTechnologiesFromProject = catchAsync(async (req, res, next) => {
    const errorMessages = validation.results(req);

    if (errorMessages.length) return next(new AppError(errorMessages, 400));

    const { technologiesIds } = req.body;

    await ProjectTechnologies.deleteMany(req.record.id, technologiesIds);

    return res.status(204).json({
        status: 'success',
        data: null
    });
});

exports.getAllTechnologiesFromProject = catchAsync(async (req, res) => {
    const projectsTechnologies = await ProjectTechnologies.findMany(req.record.id);
    
    return res.status(200).json({
        status: 'success',
        data: projectsTechnologies
    });
});
