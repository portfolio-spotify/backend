const morgan = require('morgan');

const logger = require('./logger');

const getMiddleware = () => {
    if (process.env.NODE_ENV === 'production') {
        const middleware = morgan(
            (tokens, req, res) => {
                const data = JSON.stringify({
                    request_id: req.id,
                    method: tokens.method(req, res),
                    url: tokens.url(req, res),
                    status: Number.parseFloat(tokens.status(req, res)),
                    response_time: Number.parseFloat(tokens['response-time'](req, res)) + ' ms',
                    user_agent: tokens['user-agent'](req, res)
                });
        
                return data;
            },
            {
                stream: {
                    write: (message) => {
                        const data = JSON.parse(message);
        
                        logger.info('Incoming request', data);
                    }
                }
            }
        );

        return middleware;
    }

    const middleware = morgan('dev');

    return middleware;
};

module.exports = getMiddleware();