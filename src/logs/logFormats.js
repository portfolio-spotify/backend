const winston = require('winston');
const { combine, timestamp, json, errors, label, prettyPrint } = winston.format;

exports.logFormatDev = combine(
    winston.format((info) => {
        info.level = info.level.toUpperCase();
        return info;
    })(),
    label({ label: '[LOGGER]' }),
    timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    prettyPrint({ colorize: true }),
);

exports.logFormatProd = combine(
    errors({ stack: true }),
    timestamp(),
    json()
);
