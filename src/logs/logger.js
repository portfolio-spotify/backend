const winston = require('winston');

const { logFormatDev, logFormatProd } = require('./logFormats');
const googleCloudLogging = require('../configs/googleCloudLogging');

const getLogger = () => {
    if (process.env.NODE_ENV === 'production') {
        return winston.createLogger({
            level: 'info',
            format: logFormatProd,
            transports: [
                googleCloudLogging
            ],
            exitOnError: false,
        });
    }

    return winston.createLogger({
        level: 'silly',
        format: logFormatDev,
        transports: [
            new winston.transports.Console(),
        ],
        exitOnError: false,
    });
};

module.exports = getLogger();
