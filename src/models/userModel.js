const prisma = require('../configs/prisma');

const User = prisma.users;

const imageUrl = process.env.AWS_S3_PORTFOLIO_URL;
const imageFolders = process.env.AWS_S3_USERS_IMAGES_PATH;
const imagesPath = `${imageUrl}${imageFolders}`;

exports.findOne = async (id) => {
    const user = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.first_name,
            t1.last_name,
            t1.description,
            t1.url,
            t1.color,
            t1.image_profile,
            t1.image_cover,
            t1.image_description,
            CONCAT(${imagesPath}, t1.image_profile) AS "image_profileURL",
            CONCAT(${imagesPath}, t1.image_cover) AS "image_coverURL",
            CONCAT(${imagesPath}, t1.image_description) AS "image_descriptionURL"
        FROM portfolio.users t1
        WHERE t1.url = ${id}
    `;

    return user[0];
};

exports.findMany = async () => {
    const users = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.first_name,
            t1.last_name,
            t1.description,
            t1.url,
            t1.color,
            t1.image_profile,
            t1.image_cover,
            t1.image_description,
            CONCAT(${imagesPath}, t1.image_profile) AS "image_profileURL",
            CONCAT(${imagesPath}, t1.image_cover) AS "image_coverURL",
            CONCAT(${imagesPath}, t1.image_description) AS "image_descriptionURL"
        FROM portfolio.users t1
    `;

    return users;
};

exports.createOne = async (data) => {
    const {
        firstName,
        lastName,
        description,
        url,
        color,
        imageProfile,
        imageCover,
        imageDescription
    } = data;

    const user = await User.create({
        data: {
            first_name: firstName,
            last_name: lastName,
            description: description,
            url: url,
            color: color,
            image_profile: imageProfile,
            image_cover: imageCover,
            image_description: imageDescription
        }
    });

    return user;
};

exports.updateOne = async (id, data) => {
    const {
        firstName,
        lastName,
        description,
        url,
        color,
        imageProfile,
        imageCover,
        imageDescription
    } = data;

    const user = await User.update({
        where: {
            url: id
        },
        data: {
            first_name: firstName,
            last_name: lastName,
            description: description,
            url: url,
            color: color,
            image_profile: imageProfile,
            image_cover: imageCover,
            image_description: imageDescription
        }
    });

    return user;
};
