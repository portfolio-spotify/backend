const { DeleteObjectCommand, GetObjectCommand } = require('@aws-sdk/client-s3');

const logger = require('../logs/logger');
const { S3Client } = require('../configs/aws');

/* 
    An utility to log response from AWS S3.
*/
exports.logResponse = (response, requestId) => {
    if (!response.$metadata) {
        logger.error('An error occured in the delete response AWS S3', {
            request_id: requestId,
            aws_response: response
        });

        return;
    }

    const statusCode = response.$metadata.httpStatusCode;

    if (statusCode >= 100 && statusCode <= 199) {
        logger.warn('An error occured in the delete process AWS S3', {
            request_id: requestId,
            aws_response: response
        });

        return;
    }

    if (statusCode >= 400 && statusCode <= 599) {
        logger.error('An error occured in the delete process AWS S3', {
            request_id: requestId,
            aws_response: response
        });

        return;
    }

    logger.warn('A record has been deleted from AWS S3', {
        request_id: requestId,
        aws_response: response
    });

    return;
};

exports.deleteObject = async (bucket, key) => {
    const command = new DeleteObjectCommand({
        Bucket: bucket,
        Key: key
    });

    const response = await S3Client.send(command);

    return response;
};

exports.getObject = async (bucket, key) => {
    const command = new GetObjectCommand({
        Bucket: bucket,
        Key: key
    });

    const response = await S3Client.send(command);

    return response;
};
