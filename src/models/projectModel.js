const prisma = require('../configs/prisma');

const Project = prisma.projects;

const imageUrl = process.env.AWS_S3_PORTFOLIO_URL;
const imageFolders = process.env.AWS_S3_PROJECTS_IMAGES_PATH;
const imagesPath = `${imageUrl}${imageFolders}`;

exports.findOne = async (id) => {
    const project = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL",
            t2.url AS "author_id"
        FROM portfolio.projects t1
        INNER JOIN portfolio.users t2
        ON t1.author_id = t2.id
        WHERE t1.url = ${id}
    `;

    return project[0];
};

exports.findMany = async () => {
    const projects = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL",
            t2.url AS "author_id"
        FROM portfolio.projects t1
        INNER JOIN portfolio.users t2
        ON t1.author_id = t2.id
    `;

    return projects;
};

exports.createOne = async (data) => {
    const { title, description, image, color, url, author_id } = data;

    const project = await Project.create({
        data: {
            title,
            description,
            image,
            color,
            url,
            author_id
        }
    });

    return project;
};

exports.updateOne = async (id, data) => {
    const { title, description, image, color, url } = data;

    const project = await Project.update({
        where: {
            url: id
        },
        data: {
            title,
            description,
            image,
            color,
            url
        }
    });

    return project;
};

exports.deleteOne = async (id) => {
    const project = await Project.delete({
        where: {
            url: id
        }
    });

    return project;
};

exports.findRandom = async (limit) => {
    const random = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.author_id,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL" 
        FROM portfolio.projects t1
        ORDER BY RANDOM()
        LIMIT ${limit}
    `;

    return random;
};

exports.findLatest = async (limit) => {
    const latest = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.author_id,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL" 
        FROM portfolio.projects t1
        ORDER BY t1.created_at DESC
        LIMIT ${limit}
    `;

    return latest;
};

exports.findDiscover = async (limit) => {
    const discover = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.author_id,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL",
            CAST(SUM(t2.play_count) AS INTEGER) AS "total_listened"
        FROM portfolio.projects t1
        INNER JOIN portfolio.tracks t2
        ON t1.id = t2.project_id
        GROUP BY t1.id
        ORDER BY total_listened
        LIMIT ${limit}
    `;

    return discover;
};

exports.findMostListenedByTechnology = async (technologyId, limit) => {
    const mostListened = await prisma.$queryRaw`
        SELECT
            t1.id,
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.author_id,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL",
            CAST(SUM(t4.play_count) AS INTEGER) AS "total_listened"
        FROM portfolio.projects t1
        INNER JOIN portfolio.projects_technologies t2
        ON t1.id = t2.project_id
        INNER JOIN portfolio.technologies t3
        ON t2.technology_id = t3.id
        INNER JOIN portfolio.tracks t4
        ON t1.id = t4.project_id
        WHERE t3.url = ${technologyId}
        GROUP BY t1.id, t3.id
        ORDER BY total_listened DESC
        LIMIT ${limit}
    `;

    return mostListened;
};

exports.findLatestByTechnology = async (technologyId, limit) => {
    const latest = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.author_id,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL" 
        FROM portfolio.projects t1
        INNER JOIN portfolio.projects_technologies t2
        ON t1.id = t2.project_id
        INNER JOIN portfolio.technologies t3
        ON t2.technology_id = t3.id
        WHERE t3.url = ${technologyId}
        ORDER BY t1.created_at DESC
        LIMIT ${limit}
    `;

    return latest;
};

exports.findMostListenedByUser = async (userId, limit) => {
    const mostListened = await prisma.$queryRaw`
        SELECT
            t1.id,
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.author_id AS "user_id",
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL",
            CAST(SUM(t4.play_count) AS INTEGER) AS "total_listened",
            t2.url AS "author_id"
        FROM portfolio.projects t1
        INNER JOIN portfolio.users t2
        ON t1.author_id = t2.id
        INNER JOIN portfolio.tracks t4
        ON t1.id = t4.project_id
        WHERE t2.url = ${userId}
        GROUP BY t1.id, t2.url
        ORDER BY total_listened DESC
        LIMIT ${limit}
    `;

    return mostListened;
};

exports.findLatestByUser = async (userId, limit) => {
    const latest = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.title, 
            t1.description, 
            t1.color,
            t1.created_at,
            t1.url,
            t1.author_id AS "user_id",
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL",
            t2.url AS "author_id"
        FROM portfolio.projects t1
        INNER JOIN portfolio.users t2
        ON t1.author_id = t2.id
        WHERE t2.url = ${userId}
        ORDER BY t1.created_at DESC
        LIMIT ${limit}
    `;

    return latest;
};

exports.findTotalByUser = async (userId) => {
    const total = await prisma.$queryRaw`
        SELECT 
            CAST(COUNT(*) AS INTEGER) AS "total"
        FROM portfolio.projects t1
        INNER JOIN portfolio.users t2
        ON t1.author_id = t2.id
        WHERE t2.url = ${userId}
    `;

    return total[0];
};
