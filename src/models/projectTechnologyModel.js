const prisma = require('../configs/prisma');

const ProjectTechnology = prisma.projects_technologies;

exports.createMany = async (projectId, technologiesIds) => {
    const promisesToInsert = technologiesIds.map((technologyId) => {
        const promise = ProjectTechnology.create({
            data: {
                project_id: projectId,
                technology_id: technologyId
            }
        });

        return promise;
    });

    const projectsTechnologies = await Promise.all(promisesToInsert);

    return projectsTechnologies;
};

exports.deleteMany = async (projectId, technologiesIds) => {
    const promisesToDelete = technologiesIds.map((technologyId) => {
        const promise = ProjectTechnology.delete({
            where: {
                project_id_technology_id: {
                    project_id: projectId,
                    technology_id: technologyId
                }
            }
        });

        return promise;
    });

    const projectsTechnologies = await Promise.all(promisesToDelete);

    return projectsTechnologies;
};

exports.findMany = async (projectId) => {
    const projectsTechnologies = await ProjectTechnology.findMany({
        where: {
            project_id: projectId
        }
    });

    return projectsTechnologies;
};
