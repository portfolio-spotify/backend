const prisma = require('../configs/prisma');
const { generateId } = require('../utils/id');

const Track = prisma.tracks;

const assetsUrl = process.env.AWS_S3_PORTFOLIO_URL;
const audioFolders = process.env.AWS_S3_TRACKS_PATH;
const audioPath = `${assetsUrl}${audioFolders}`;

const projectImageUrl = process.env.AWS_S3_PORTFOLIO_URL;
const projectImageFolder = process.env.AWS_S3_PROJECTS_IMAGES_PATH;
const projectImagePath = `${projectImageUrl}${projectImageFolder}`;

exports.findOne = async (id) => {
    const track = await prisma.$queryRaw`
        SELECT
            t1.id,
            t1.title,
            t1.play_count,
            t1.created_at,
            t1.url,
            t1.project_id,
            t1.audio_file,
            t1.order_number,
            t1.duration,
            CONCAT(${audioPath}, t1.audio_file) AS "audioFileURL"
        FROM portfolio.tracks t1
        WHERE t1.url = ${id}
    `;

    return track[0];
};

exports.findMany = async () => {
    const tracks = await prisma.$queryRaw`
        SELECT
            t1.id,
            t1.title,
            t1.play_count,
            t1.created_at,
            t1.url,
            t1.project_id,
            t1.audio_file,
            t1.order_number,
            t1.duration,
            CONCAT(${audioPath}, t1.audio_file) AS "audioFileURL"
        FROM portfolio.tracks t1
        ORDER BY t1.order_number
    `;

    return tracks;
};

exports.createOne = async (data) => {
    const { title, audio_file, projectId, orderNumber, duration } = data;

    const url = generateId(25);
    const play_count = 0;

    const track = await Track.create({
        data: {
            title,
            play_count,
            url,
            audio_file,
            duration,
            project_id: projectId,
            order_number: orderNumber
        }
    });

    return track;
};

exports.updateOne = async (id, data) => {
    const { title, play_count, audio_file, project_id, order_number, duration } = data;

    const track = await Track.update({
        where: {
            url: id
        },
        data: {
            title,
            play_count,
            audio_file,
            project_id,
            order_number,
            duration
        }
    });

    return track;
};

exports.deleteOne = async (id) => {
    const track = await Track.delete({
        where: {
            url: id
        }
    });

    return track;
};

exports.findManyFromProject = async (projectId) => {
    const tracks = await prisma.$queryRaw`
        SELECT
            t1.id,
            t1.title,
            t1.play_count,
            t1.created_at,
            t1.url,
            t1.project_id,
            t1.audio_file,
            t1.order_number,
            t1.duration,
            t3.first_name,
            t3.last_name,
            CONCAT(${audioPath}, t1.audio_file) AS "audioFileURL"
        FROM portfolio.tracks t1
        INNER JOIN portfolio.projects t2
        ON t1.project_id = t2.id
        INNER JOIN portfolio.users t3
        ON t2.author_id = t3.id
        WHERE t1.project_id = ${projectId}
        ORDER BY t1.order_number
    `;

    return tracks;
};

exports.findMostListenedByUser = async (userId, limit) => {
    const mostListened = await prisma.$queryRaw`
        SELECT
            t1.id,
            t1.title,
            t1.play_count,
            t1.created_at,
            t1.url,
            t1.project_id,
            t1.audio_file,
            t1.order_number,
            t1.duration,
            CONCAT(${audioPath}, t1.audio_file) AS "audioFileURL",
            CONCAT(${projectImagePath}, t2.image) AS "imageURL",
            t2.description
        FROM portfolio.tracks t1
        INNER JOIN portfolio.projects t2
        ON t1.project_id = t2.id
        INNER JOIN portfolio.users t3
        ON t2.author_id = t3.id
        WHERE t3.url = ${userId}
        ORDER BY t1.play_count DESC
        LIMIT ${limit}
    `;

    return mostListened;
};
