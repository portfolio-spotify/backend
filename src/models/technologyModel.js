const prisma = require('../configs/prisma');

const Technology = prisma.technologies;

const imageUrl = process.env.AWS_S3_PORTFOLIO_URL;
const imageFolders = process.env.AWS_S3_TECHNOLOGIES_IMAGES_PATH;
const imagesPath = `${imageUrl}${imageFolders}`;

exports.findOne = async (id) => {
    const technology = await prisma.$queryRaw`
        SELECT 
            t1.id,
            t1.name,
            t1.url,
            t1.color,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL"
        FROM portfolio.technologies t1
        WHERE t1.url = ${id}
    `;

    return technology[0];
};

exports.findMany = async () => {
    const technologies = await prisma.$queryRaw`
        SELECT 
            t1.id, 
            t1.name, 
            t1.url, 
            t1.color,
            t1.image,
            CONCAT(${imagesPath}, t1.image) AS "imageURL",
            CAST(COUNT(t1.id) AS INTEGER) AS "total_usage"
        FROM portfolio.technologies t1
        INNER JOIN portfolio.projects_technologies t2
        ON t1.id = t2.technology_id
        GROUP BY t1.id
    `;

    return technologies;
};

exports.createOne = async (data) => {
    const { name, url, color, image } = data;

    const technology = await Technology.create({
        data: {
            name,
            url,
            color,
            image
        }
    });

    return technology;
};

exports.updateOne = async (id, data) => {
    const { name, url, color, image } = data;

    const technology = await Technology.update({
        where: {
            url: id
        },
        data: {
            name,
            url,
            color,
            image
        }
    });

    return technology;
};

exports.deleteOne = async (id) => {
    const technology = await Technology.delete({
        where: {
            url: id
        }
    });

    return technology;
};
