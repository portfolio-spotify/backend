INSERT INTO portfolio.technologies (name, url, color, image)
VALUES 
('PostgreSQL', 'postgresql', '#336791', 'image.jpg'), 
('Node.js', 'node', '#679E63', 'image.jpg'),
('React.js', 'react', '#61DAFB', 'image.jpg'),
('MySQL', 'mysql', '#00758F', 'image.jpg'),
('Javascript', 'javascript', '#F7E018', 'image.jpg'),
('Redis', 'redis', '#CD5D57', 'image.jpg'),
('PHP', 'php', '#777BB3', 'image.jpg'),
('CSS', 'css', '#264DE4', 'image.jpg'),
('Nginx', 'nginx', '#009639', 'image.jpg'),
('SASS', 'sass', '#CF649A', 'image.jpg'),
('Express.js', 'express', '#181818', 'image.jpg'),
('HTML', 'html', '#E44D25', 'image.jpg'),
('Figma', 'figma', '#A259FF', 'image.jpg'),
('MongoDB', 'mongodb', '#00ED64', 'image.jpg'),
('Mongoose', 'mongoose', '#880000', 'image.jpg'),
('Typescript', 'typescript', '#3178C6', 'image.jpg')
;
