CREATE SCHEMA portfolio;

CREATE TABLE portfolio.users (
    id SERIAL,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    description TEXT NOT NULL,
    url TEXT NOT NULL UNIQUE,
    color TEXT NOT NULL,
    image_profile TEXT,
    image_cover TEXT,
    image_description TEXT,
    PRIMARY KEY (id)
);

CREATE TABLE portfolio.projects (
    id SERIAL,
    title TEXT NOT NULL,
    description TEXT NOT NULL,
    image TEXT,
    color TEXT NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    url TEXT NOT NULL UNIQUE,
    author_id INT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (author_id) REFERENCES portfolio.users (id) ON DELETE CASCADE
);

CREATE TABLE portfolio.tracks (
    id SERIAL,
    title TEXT NOT NULL,
    order_number INT NOT NULL,
    play_count INT NOT NULL DEFAULT 0,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    url TEXT NOT NULL UNIQUE,
    audio_file TEXT NOT NULL,
    project_id INT NOT NULL,
    duration DECIMAL NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (project_id) REFERENCES portfolio.projects (id) ON DELETE CASCADE
);

CREATE TABLE portfolio.technologies (
    id SERIAL,
    name TEXT NOT NULL UNIQUE,
    url TEXT NOT NULL UNIQUE,
    color TEXT NOT NULL,
    image TEXT,
    PRIMARY KEY (id)
);

CREATE TABLE portfolio.projects_technologies (
    project_id INT,
    technology_id INT,
    PRIMARY KEY (project_id, technology_id),
    FOREIGN KEY (project_id) REFERENCES portfolio.projects (id) ON DELETE CASCADE,
    FOREIGN KEY (technology_id) REFERENCES portfolio.technologies (id) ON DELETE CASCADE
);
