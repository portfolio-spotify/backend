const { checkSchema, validationResult } = require('express-validator');

const limitSchema = {
    in: ['query'],
    customSanitizer: {
        options: (value) => {
            if (!value) return null;

            const defaultLimit = 6;

            let limit = parseInt(value || defaultLimit, 10);

            if (Number.isNaN(limit) || limit <= 0) limit = defaultLimit;

            return limit;
        }
    }
};

exports.technology = checkSchema({
    name: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Name should have a value !'
        }
    },
    url: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Url should have a value !'
        }
    },
    color: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Color should have a value !'
        }
    }
});

exports.user = checkSchema({
    firstName: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'First name should have a value !'
        }
    },
    lastName: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Last name should have a value !'
        }
    },
    description: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Description should have a value !'
        }
    },
    url: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Url should have a value !'
        }
    },
    color: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Color should have a value !'
        }
    }
});

exports.project = checkSchema({
    title: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Title should have a value !'
        }
    },
    description: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Description should have a value !'
        }
    },
    color: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Color should have a value !'
        }
    },
    url: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Url should have a value !'
        }
    },
    technologiesIds: {
        in: ['body'],
        customSanitizer: {
            options: (value, { req }) => {
                const { body } = req;

                if (!Object.prototype.hasOwnProperty.call(body, 'technologiesIds')) return [];
                
                let sanitizedValue;

                try {
                    sanitizedValue = JSON.parse(value);
                } catch (err) {
                    sanitizedValue = false;
                }
                
                return sanitizedValue;
            }
        },
        custom: {
            options: (value) => {
                if (Array.isArray(value)) return true;

                return false;
            },
            errorMessage: 'Technologies ids should be an array !'
        }
    }
});

exports.track = checkSchema({
    title: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Title should have a value !'
        }
    },
    projectId: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Project id should have a value !'
        },
        isInt: {
            errorMessage: 'Invalid project id format, please use INT !'
        },
        toInt: true
    },
    orderNumber: {
        in: ['body'],
        trim: true,
        isEmpty: {
            negated: true,
            errorMessage: 'Track should have a number !'
        },
        isInt: {
            errorMessage: 'Invalid project id format, please use INT !'
        },
        toInt: true
    }
});

exports.projectTechnology = checkSchema({
    technologiesIds: {
        in: ['body'],
        isArray: {
            errorMessage: 'Technologies ids should be an array !'
        }
    }
});

exports.section = checkSchema({
    limit: limitSchema
});

exports.technologyProject = checkSchema({
    limit: limitSchema
});

exports.userProject = checkSchema({
    limit: limitSchema
});

exports.userTrack = checkSchema({
    limit: limitSchema
});

exports.results = (req) => {
    const errorMessages = [];

    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        const messages = errors.array().map((error) => error.msg);

        errorMessages.push(...messages);

        return errorMessages;
    }

    return errorMessages;
};
