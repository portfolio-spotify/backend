const rateLimit = require('express-rate-limit');

const AppError = require('../errors/appError');

module.exports = rateLimit({
	windowMs: process.env.API_RATE_LIMITER_WINDOW * 60 * 1000,
	max: process.env.API_RATE_LIMITER_MAX_REQUESTS,
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
    message: 'Too many requests from this IP, please try again in an hour!',
    handler: (req, res, next, options) => {
		const { message, statusCode } = options;

		return next(new AppError([message], statusCode));
	},
});