const hexoid = require('hexoid');

exports.generateId = (length) => {
    const id = hexoid(length)();

    return id;
};