const { LoggingWinston } = require('@google-cloud/logging-winston');

const googleCloudLogging = new LoggingWinston({
    projectId: process.env.GOOGLE_CLOUD_LOGGING_PROJECT_ID,
    keyFilename: `${__dirname}/${process.env.GOOGLE_CLOUD_LOGGING_KEY_NAME}`
});

module.exports = googleCloudLogging;