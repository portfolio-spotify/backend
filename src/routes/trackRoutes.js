const express = require('express');

const trackController = require('../controllers/trackController');
const authController = require('../controllers/authController');

const validation = require('../utils/validation');

const router = express.Router();

router.route('/')
    .get(trackController.getAllTracks)
    .post(
        authController.protect,
        trackController.uploadTrackAudio,
        validation.track,
        trackController.createTrack
    )
;

router.route('/:id')
    .get(trackController.verifyTrack, trackController.getTrack)
    .patch(
        authController.protect,
        trackController.verifyTrack,
        trackController.uploadTrackAudio,
        validation.track,
        trackController.updateTrack
    )
    .delete(authController.protect, trackController.deleteTrack)
;

module.exports = router;