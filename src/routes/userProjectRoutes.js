const express = require('express');

const userProjectController = require('../controllers/userProjectController');
const userController = require('../controllers/userController');

const validation = require('../utils/validation');

const router = express.Router({ mergeParams: true });

router.use(userController.verifyUser);

router.route('/total').get(userProjectController.getTotalProjects);

router.use(validation.userProject);

router.route('/popular').get(userProjectController.getPopularProjectsFromUser);

router.route('/new').get(userProjectController.getNewProjectsFromUser);

module.exports = router;
