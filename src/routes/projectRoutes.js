const express = require('express');

const projectController = require('../controllers/projectController');
const authController = require('../controllers/authController');

const projectTechnologyRouter = require('./projectTechnologyRoutes');
const projectTrackRouter = require('./projectTrackRoutes');

const validation = require('../utils/validation');

const router = express.Router();

router.use('/:id/technologies', projectTechnologyRouter);
router.use('/:id/tracks', projectTrackRouter);

router.route('/')
    .get(projectController.getAllProjects)
    .post(
        authController.protect,
        projectController.uploadProjectImage,
        validation.project,
        projectController.createProject
    )
;

router.route('/:id')
    .get(projectController.verifyProject, projectController.getProject)
    .patch(
        authController.protect,
        projectController.verifyProject,
        projectController.uploadProjectImage,
        validation.project,
        projectController.updateProject
    )
    .delete(authController.protect, projectController.deleteProject)
;

module.exports = router;
