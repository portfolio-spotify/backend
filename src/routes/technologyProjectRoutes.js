const express = require('express');

const technologyProjectController = require('../controllers/technologyProjectController');
const technologyController = require('../controllers/technologyController');

const validation = require('../utils/validation');

const router = express.Router({ mergeParams: true });

router.use(validation.technologyProject);
router.use(technologyController.verifyTechnology);

router.route('/popular').get(technologyProjectController.getPopularProjectsFromTechnology);

router.route('/new').get(technologyProjectController.getNewProjectsFromTechnology);

module.exports = router;
