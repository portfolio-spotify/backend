const express = require('express');

const sectionController = require('../controllers/sectionController');

const validation = require('../utils/validation');

const router = express.Router();

router.use(validation.section);

router.route('/').get(sectionController.getAllSections);

router.route('/trending').get(sectionController.getTrendingSection);
router.route('/latest').get(sectionController.getLatestSection);
router.route('/discover').get(sectionController.getDiscoverSection);
router.route('/selected').get(sectionController.getSelectedSection);

module.exports = router;