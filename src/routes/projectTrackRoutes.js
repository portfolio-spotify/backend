const express = require('express');

const projectTrackController = require('../controllers/projectTrackController');
const projectController = require('../controllers/projectController');

const router = express.Router({ mergeParams: true });

router.route('/').get(projectController.verifyProject, projectTrackController.getAllTracksFromProject);

module.exports = router;
