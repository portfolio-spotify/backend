const express = require('express');

const userTrackController = require('../controllers/userTrackController');
const userController = require('../controllers/userController');

const validation = require('../utils/validation');

const router = express.Router({ mergeParams: true });

router.use(userController.verifyUser);
router.use(validation.userTrack);

router.route('/popular').get(userTrackController.getPopularTracksFromUser);

module.exports = router;
