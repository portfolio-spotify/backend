const express = require('express');

const technologyController = require('../controllers/technologyController');
const authController = require('../controllers/authController');

const technologyProjectRouter = require('./technologyProjectRoutes');

const validation = require('../utils/validation');

const router = express.Router();

router.use('/:id/projects', technologyProjectRouter);

router.route('/')
    .get(technologyController.getAllTechnologies)
    .post(
        authController.protect,
        technologyController.uploadTechnologyImage,
        validation.technology,
        technologyController.createTechnology,
    )
;

router.route('/:id')
    .get(technologyController.verifyTechnology, technologyController.getTechnology)
    .patch(
        authController.protect,
        technologyController.verifyTechnology,
        technologyController.uploadTechnologyImage,
        validation.technology,
        technologyController.updateTechnology
    )
    .delete(authController.protect, technologyController.deleteTechnology)
;

module.exports = router;
