const express = require('express');

const projectTechnologyController = require('../controllers/projectTechnologyController');
const projectController = require('../controllers/projectController');
const authController = require('../controllers/authController');

const validation = require('../utils/validation');

const router = express.Router({ mergeParams: true });

router.route('/')
    .get(projectController.verifyProject, projectTechnologyController.getAllTechnologiesFromProject)
    .post(
        authController.protect,
        projectController.verifyProject,
        validation.projectTechnology,
        projectTechnologyController.addTechnologiesToProject
    )
    .delete(
        authController.protect,
        projectController.verifyProject,
        validation.projectTechnology,
        projectTechnologyController.deleteTechnologiesFromProject
    )
;

module.exports = router;
