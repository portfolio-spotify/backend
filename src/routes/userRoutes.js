const express = require('express');

const userController = require('../controllers/userController');
const authController = require('../controllers/authController');

const userProjectRouter = require('./userProjectRoutes');
const userTrackRouter = require('./userTrackRoutes');

const validation = require('../utils/validation');

const router = express.Router();

router.use('/:id/projects', userProjectRouter);
router.use('/:id/tracks', userTrackRouter);

router.route('/')
    .get(userController.getAllUsers)
    .post(
        authController.protect,
        userController.uploadUserImages,
        validation.user,
        userController.createUser
    )
;

router.route('/:id')
    .get(userController.verifyUser, userController.getUser)
    .patch(
        authController.protect,
        userController.verifyUser,
        userController.uploadUserImages,
        validation.user,
        userController.updateUser
    )
;

module.exports = router;
