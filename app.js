const express = require('express');
const helmet = require('helmet');
const cookieParser = require('cookie-parser')
const compression = require('compression');

const apiLimiter = require('./src/utils/apiLimiter');
const requestId = require('./src/utils/requestId');
const httpLogger = require('./src/logs/httpLogger');

const userRouter = require('./src/routes/userRoutes');
const technologyRouter = require('./src/routes/technologyRoutes');
const projectRouter = require('./src/routes/projectRoutes');
const trackRouter = require('./src/routes/trackRoutes');
const sectionRouter = require('./src/routes/sectionRoutes');
const globalRouter = require('./src/routes/globalRoutes');

const globalErrorLogHandler = require('./src/errors/globalErrorLogHandler');
const globalErrorHandler = require('./src/errors/globalErrorHandler');

const app = express();

app.disable('x-powered-by');

app.use(requestId);
app.use(httpLogger);
app.use(helmet());

// troubleshotting proxies issues (TODO when in production)
// https://www.npmjs.com/package/express-rate-limit
// app.use('/api', apiLimiter);
app.use(express.json({ limit: '10kb' }));
app.use(express.urlencoded({ extended: true, limit: '10kb' }));
app.use(cookieParser());
app.use(compression());

app.use('/api/users', userRouter);
app.use('/api/technologies', technologyRouter);
app.use('/api/projects', projectRouter);
app.use('/api/tracks', trackRouter);
app.use('/api/sections', sectionRouter);

app.all('*', globalRouter);

app.use(globalErrorLogHandler);
app.use(globalErrorHandler);

module.exports = app;
