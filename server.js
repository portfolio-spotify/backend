const dotenv = require('dotenv');

dotenv.config({ path: './.env' });

const logger = require('./src/logs/logger');
const handlesException = require('./src/errors/handlesException');
const handlesRejection = require('./src/errors/handlesRejection');

process.on('uncaughtException', handlesException);
process.on('unhandledRejection', handlesRejection);

const app = require('./app');

const port = process.env.PORT || 4000;

app.listen(port, () => logger.info(`Server running on port ${port} 🔥`));
